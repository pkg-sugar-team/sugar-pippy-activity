Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Pippy
Upstream-Contact: https://github.com/sugarlabs/Pippy/issues
 Sugarlabs <sugar-devel@lists.sugarlabs.org>
 http://lists.sugarlabs.org/listinfo/sugar-devel
 irc://irc.freenode.net/sugar
Source: https://github.com/sugarlabs/Pippy

Files: *
Copyright:
  2007-2009        Chris Ball
  2007             Collabora Ltd
  2013-2014        Ignacio Rodríguez <ignacio@sugarlabs.org>
  2013             Jorge Gomez
  2007-2008, 2010  One Laptop Per Child Asociation, Inc
  2006             Red Hat, Inc.
  2013-2014        Sai Vineet
  2015             Sam Parkinson
  2013-2015        Walter Bender <walter@sugarlabs.org>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
License: GPL-2+

Files: library/pippy/physics/myelements/*
Copyright:
  2008  The Elements Team, <elements@linuxuser.at>
License-Grant:
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
License: GPL-3+

Files: texteditor.py
Copyright:
  2015  Batchu Venkat Vishal
License-Grant:
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
License: LGPL-2+

Files: debian/*
Copyright:
  2008-2010, 2012, 2015-2021, 2024  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version.
License: GPL-3+
Reference:
 debian/copyright

License: GPL-2+
Reference: /usr/share/common-licenses/GPL-2

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3

License: LGPL-2+
Reference: /usr/share/common-licenses/LGPL-2
