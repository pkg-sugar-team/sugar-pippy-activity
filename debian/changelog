sugar-pippy-activity (76-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.6.2
  * simplify source helper script copyright-check
  * clean generated locale files;
    closes: bug#1046811, thanks to Lucas Nussbaum
  * update copyright info:
    + drop duplicate source URI
    + update coverage
    + use field Reference

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 06 Feb 2024 10:38:47 +0100

sugar-pippy-activity (75-2) unstable; urgency=medium

  * simplify build;
    build-depend on dh-linktree dh-sequence-python3
    (not cdbs dh-python)
  * drop obsolete workaround
    for build path leaking into installed files;
    tighten build-dependency on python3-sugar3
  * use debhelper compatibility level 12 (not 10);
    build-depend on debhelper-compat (not debhelper)
  * declare compliance with Debian Policy 4.5.1
  * fix shebang

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 03 Feb 2021 00:39:22 +0100

sugar-pippy-activity (75-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * copyright: stop repackage upstream source:
    no longer includes convenience code copies of external code projects
  * stop rename ISO 639-2 locale ibo: dropped upstream
  * use code copy of python-elements (dead upstream);
    drop patch 2001;
    stop suggest python-elements
  * use debhelper compatibility level 10 (not 9)
  * declare compliance with Debian Policy 4.5.0
  * watch:
    + simplify regular expressions;
    + rewrite usage comment;
    + use substitution strings;
    + use dversionmangle=auto
  * depend on gir1.2-telepathyglib-0.12 (not python3-telepathy)
  * (build-)depend on python3 modules (not python);
    build-depend on dh-python;
    closes: bug#912501, #938592,
    thanks to Dominik George and Matthias Klose

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 18 Apr 2020 15:37:03 +0200

sugar-pippy-activity (72~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Simplify rules.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Update Vcs-* URLs: Maintenance moved to Salsa.
  * Set Rules-Requires-Root: no.
  * Update copyright info:
    + Use https protocol in alternate Source URL.
    + Strip superfluous copyright signs.
    + Extend coverage of packaging.
    + Stop track files no longer shipped upstream.
  * Update copyright check script.
  * Suggest gir1.2-gstreamer-1.0 gstreamer1.0-plugins-base
    gstreamer1.0-plugins-good
    (not python-gst0.10 gstreamer0.10-plugins-base
    gstreamer0.10-plugins-good).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 02 Mar 2019 23:45:20 +0100

sugar-pippy-activity (71~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New upstream release:
    + Add Gtk and Sugar examples.
    + Merge Albanian strings.
    + Merge Yoruba strings.
    + honor max-participants.
    + Copy from Vte or text buffer.
    + Rename example load to open.
    + Load examples in new tab.
    + Typo, disutils to distutils.
    + Save as library; only if writable.
    + Load example; omit En and Es directories.
    + Pong example playability.
    + Repair Gtk tutorial examples.
    + Fix missing run, stop, clear icons.
    + Use either Vte version.
    + Use latest collabwrapper.py.

  [ Jonas Smedegaard ]
  * Modernize cdbs:
    + Drop upstream-tarball hints: Use gbp import-orig --uscan.
    + Do copyright-check in maintainer script (not during build).
      Relax to build-depend unversioned on cdbs.
      Stop build-depend on licensecheck.
  * Modernize Vcs-* fields: Use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Update copyright info:
    + Extend coverage for myself.
    + Use https protocol in file format URL.
  * Fix rename locale ibo→ig: ISO 639-1 favored over ISO 639-2.
  * Add DEP-3 headers to patch.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 Aug 2017 22:42:53 +0200

sugar-pippy-activity (70+20160925~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * Git snapshot.
    Highlights:
    + use latest CollabWrapper.
    + Use either Vte version.
    + Save as library; only if writable.
    + Typo, disutils to distutils.

  [ Jonas Smedegaard ]
  * Update copyright info: Update URLs to reflect new Github home.
  * Use get-orig-source for git snapshots. Document in README.source.
  * Modernize CDBS use: Build-depend on licensecheck (not devscripts).
  * Fix broken paths in desktop file.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 13 Dec 2016 16:01:46 +0100

sugar-pippy-activity (70~dfsg-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    67
    + Updated VTE and Box2D support; cleaned up support for ARM.
    68
    + New translations.
    + Fixes to notebook tabs.
    69
    + Add missing file.
    70
    + More collabwrapper fixes.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Use only github (not sugarlabs.org) as source.
    + Add Github issue tracker as preferred contact.
    + Repackage upstream tarball, excluding non-DFSG binaries and
      convenience code copies of pybox2d.
    + Extend copyright of packaging to cover current year.
    + Extend to cover newly added files.
  * Update watch file:
    + Bump to file format 4.
    + Watch github (not sugarlabs.org) and mangle filename of download.
    + Mention gbp --uscan in usage comment.
    + Set repacksuffix and mangle debian version.
  * Drop CDBS get-orig-source target: Use "gbp import-orig --uscan"
    instead.
  * Update git-buildpackage config: Filter any .git* file.
  * Use https protocol in Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 09 May 2016 10:28:47 +0200

sugar-pippy-activity (66-4) unstable; urgency=medium

  * Update package relations:
    + Fix relax to suggest (not depend on) python-elements: Needed only
      for one sample code (graphics/physics).
    + Relax to suggest (not recommend) python-gst0.10.
    + Fix suggest gstreamer1.0-plugins-base gstreamer0.10-plugins-good.
  * Fix rename locale aym→ay: ISO 639-1 favored over ISO 639-2.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 13 Jul 2015 15:05:56 +0200

sugar-pippy-activity (66-3) unstable; urgency=medium

  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 26 Jun 2015 13:19:59 -0500

sugar-pippy-activity (66-2) experimental; urgency=medium

  * Update package relations:
    + Recommend/depend on unbranched Glucose parts (i.e. core
      libraries).

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 19 Jun 2015 16:51:36 -0500

sugar-pippy-activity (66-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Rewrite README.source to emphasize that control.in file is *not* a
    show-stopper for contributions, and refer to wiki page for details.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage for myself.
    + Relicense packaging as GPL-3+.
    + Update alternate git source URL.
  * Add lintian overrides regarding license in License-Reference field.
    See bug#786450.
  * Update package relations:
    + Fix suppress build-dependency on python-dev future-compatibly with
      cdbs 0.5.
    + Fix explicitly depend on python.
    + Relax slightly build-dependency on cdbs.
    + Relax to build-depend unversioned on python: Needed version
      satisfied even in oldstable.
    + (Build-)depend on python-sugar3 (not python-sugar or
      python-sugar-toolkit-*).
    + Depend on python-gir and some GIR bindings, instead of some Python
      libraries.
    + Bump primary branch to 0.104.
  * Declare compliance with Debian Policy 3.9.6.
  * Move packaging to Debian Sugar Team.
  * Modernize short and long description.
  * Bump debhelper compatibility level to 8.
  * Stop repackage upstream tarball: No source-less binary since v51.
  * Update patch 2001.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 19 Jun 2015 16:25:32 -0500

sugar-pippy-activity (46~dfsg-2) unstable; urgency=low

  * Simplify setting main branch using new DEB_SUGAR_PRIMARY_BRANCH.
  * Suggest (not recommend) sugar-session.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 28 May 2012 21:03:41 +0200

sugar-pippy-activity (46~dfsg-1) unstable; urgency=low

  * New upstream release.

  * Remove debian/source/local-options: abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.
  * Bump standards-version to 3.9.3.
  * Use Python helper python2 (not python-central).
    Closes: bug#617101. Thanks to Matthias Klose.
  * Refer to FSF website (not postal address) in licensing header for
    rules file.
  * Bump debhelper compatibility level to 7.
  * Use anonscm.debian.org for Vcs-Browser field.
  * Sort package relations in control file.
  * Modernize long description, based on wiki.sugarlabs.org phrases.
  * Modernize short description based on upstream code comment.
  * Rewrite copyright file using format 1.0.
  * Extend copyright of Debian packaging to cover recent years.
  * Fix document repackaging in copyright file.
  * Update package relations:
    + Tighten build-dependency on cdbs and python: Needed for python2
      helper.
    + Stop build-depending on python-central: No longer used.
    + Relax build-depend unversioned on debhelper and devscripts: Needed
      versions satisfied even in oldstable.
    + Depend on python-elements (not python-box2d), to reflect upstream
      change.
    + Fix stop needlessly depending on sugar-presence-service.
    + Tighten dependency on python-vte.
      Closes: bug#556116. Thanks to Sascha Silbe.
  * Add patch 2001 to use system python-elements.
  * Fix limit repackaging to only strip python-elements (not outer
    pippy wrapper).
    Closes: bug#600395. Thanks esp. to James Cameron.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 May 2012 20:59:59 +0200

sugar-pippy-activity (37~dfsg-1) unstable; urgency=low

  * New upstream release.
  * Bump Policy compliance to standards-version 3.9.1.
  * Depend (not only build-depend) on branched sugar parts favored over
    virtual ones. Thanks to lintian.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 12 Oct 2010 00:42:05 +0200

sugar-pippy-activity (36~dfsg-1) unstable; urgency=low

  * New upstream release.
  * Repackage source with Pippy library (containing potentially
    sourceless binary code) stripped.
  * Drop all local CDBS snippets: included with main cdbs package now.
  * Switch to source format "3.0 (quilt)".
  * Drop patchsys-quilt.mk snippet: superfluous with source format "3.0
    (quilt)".
  * Add git-buildpackage configfile, enable signed-tags and use of
    pristine-tar.
  * Add README.source. Drop README.cdbs-tweaks, README.packaging and
    CDBS hints in debian/rules.
  * Update dependencies:
    + Handle binary (not only build-) dependencies in debian/rules.
    + Tighten build-dependency on debhelper.
    + Drop build-dependencies on quilt and patchutils.
    + Relax build-dependencies on python-central and python-dev.
    + Relax build-dependency on devscripts slightly, to ease
      backporting.
    + Tighten build-dependency on cdbs.
    + Depend on python-dbus and python-box2d.
    + Recommend sugar-session (not sugar).
    + Recommend python-carquinyol (not python-olpc-datastore).
    + Suggest csound, and note its relation to python-tamtam-common.
  * Update copyright and licensing info:
    + Add proper copyright header to debian/rules.
    + Rewrite debian/copyright using DEP5 rev54 machine-readable format.
    + Refer to sugarlabs.org (not laptop.org) as upstream URLs.
    + Add new license for some parts: LGPL-2+.
  * Update watch file, and Homepage stanza in debian/rules, to use
    sugarlabs.org (not laptop.org) URLs.
  * Update OLPC relation (now historic) in long description.
  * Update debian/rules to use sugarlabs (not laptop.org) URL.
  * Refresh patch.
  * Bump Policy compliance to standards-version to 3.9.0.
  * Fix build-depend in non-virtual packages.
    Closes: bug#549729, thanks to Lucas Nussbaum and Hideki Yamane.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 Jul 2010 20:37:14 +0200

sugar-pippy-activity (25-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Depend on ${misc:Depends}.
  * Adjust upstream source in debian/control, debian/copyright and in
    watch file.
  * Update local CDBS snippets:
    + python-sugar.mk: Install into right path. Closes: bug#507328.
    + python-sugar.mk: Install as non-localized activity name.
    + python-sugar.mk: Dynamically update locale entries in MANIFEST.
  * Semi-auto-update debian/control to tighten build-dependencies.

  [ Luke Faraone ]
  * Fix activity install path.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 07 Dec 2008 20:38:53 +0100

sugar-pippy-activity (25-1) unstable; urgency=low

  * New upstream release.
  * Update cdbs snippets:
    + Move dependency cleanup to new local snippet package-relations.mk.
    + Update README.cdbs-tweaks.
  * Update-copyright-check: PO files added (no new owners, copyrights or
    licenses).
  * Add DEB_MAINTAINER_MODE in debian/rules (thanks to Romain Beauxis).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 16 Aug 2008 21:06:42 +0200

sugar-pippy-activity (22-1) unstable; urgency=medium

  * New upstream release.
    + Properly Supports recent versions of gtksourceview.  Closes:
      bug#482120, thanks to C. Scott Ananian and others.
  * Drop patch 1002, applied upstream now.
  * Unfuzz patch 1001.
  * Add watch file.
  * Update debian/copyright:
    + Add URL to upstream tarball source.
    + Use exact URL for upstream Git source.
    + Reorganize to only mention each license once.
  * Update local cdbs snippets:
    + Use upstream-tarball.mk to track upstream source releases.
    + Adjust dependency cleanup (upstream-tarball.mk needs newer cdbs).
    + Fix python-sugar.mk to work with recent python-sugar-toolkit where
      MANIFEST files are ignored and an internal hardcoded exception
      list is used instead.  Tighten build-dependency.  Drop use of (now
      irrelevant) DEB_PYTHON_SUGAR_MANIFEST.
    + Simplify build-dependency cleanup (1-revision fix no longer
      needed).
    + Restructure output of copyright-check.mk to match new proposed
      copyright-format at
      http://wiki.debian.org/Proposals/CopyrightFormat .
      This closes: bug#487002, thanks to Lucas Nussbaum.
  * Adjust debian/copyright to follow newest proposed draft of
    http://wiki.debian.org/Proposals/CopyrightFormat .
  * Semi-auto-update debian/control to update build-dependencies:
      DEB_AUTO_UPDATE_DEBIAN_CONTROL=yes fakeroot debian/rules clean
  * Set urgency=medium to hopefully reach Lenny before freezing.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Jul 2008 18:10:20 +0200

sugar-pippy-activity (20~git.20080321-3) unstable; urgency=low

  * Update cdbs tweaks:
    + Clean *.xo and locale in python-sugar.mk (based on initial work by
      Santiago Ruano Rincón).
    + Fix preserve upstream MANIFEST files only once in python-sugar.mk.
    + Support multiple activity packages from one source in
      python-sugar.mk.
    + Update copyright-check cdbs snippet to store newline-delimited
      hints and strip any non-printable characters.  Update
      copyright-hints.
    + Drop wget options broken with recent versions of wget in
      update-tarball.mk.
    + Fix simultanously setting DEB_PYTHON_SUGAR_MANIFEST and
      DEB_PYTHON_SUGAR_MANIFEST_REGEX in python-sugar.mk.
    + Relax python-central and python-support build-dependencies in
      python-sugar.mk.
    + Tighten python-sugar.mk build-dependency on python-sugar-toolkit
      to versions using full path, and properly installs zip-based
      bundles (*.xo files).
  * Bump debhelper compatibility level to version 6.
  * Semi-auto-update debian/control to update build-dependencies:
      DEB_AUTO_UPDATE_DEBIAN_CONTROL=yes fakeroot debian/rules clean

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 26 Apr 2008 19:16:49 +0200

sugar-pippy-activity (20~git.20080321-2) unstable; urgency=low

  * Suggest (not recommend) sugar-tamtam: used for few examples, and is
    not yet in Debian (so upsets ftpmasters, as fulfilling all
    recommends is a release goal for Lenny).

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 03 Apr 2008 16:16:24 +0200

sugar-pippy-activity (20~git.20080321-1) unstable; urgency=low

  * Initial release. Closes: bug#444021.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Mar 2008 23:31:02 +0100
