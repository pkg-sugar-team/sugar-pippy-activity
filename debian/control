Source: sugar-pippy-activity
Section: x11
Priority: optional
Maintainer: Debian Sugar Team <pkg-sugar-devel@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 debhelper-compat (= 12),
 dh-linktree,
 dh-sequence-python3,
 python3,
 python3-sugar3 (>= 0.118-3~),
 unzip,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/pkg-sugar-team/sugar-pippy-activity.git
Vcs-Browser: https://salsa.debian.org/pkg-sugar-team/sugar-pippy-activity
Homepage: http://wiki.sugarlabs.org/go/Activities/Pippy
Rules-Requires-Root: no

Package: sugar-pippy-activity
Architecture: all
Depends:
 gir1.2-gdkpixbuf-2.0,
 gir1.2-glib-2.0,
 gir1.2-gtk-3.0,
 gir1.2-gtksource-3.0,
 gir1.2-pango-1.0,
 gir1.2-telepathyglib-0.12,
 gir1.2-vte-2.91,
 python3,
 python3-dbus,
 python3-jarabe,
 python3-pygame,
 python3-sugar3,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 python3-carquinyol,
Suggests:
 csound,
 gir1.2-gstreamer-1.0,
 gstreamer1.0-plugins-base,
 gstreamer1.0-plugins-good,
 python3-tamtam-data,
 sugar-session,
Provides:
 ${python3:Provides},
Description: Sugar Learning Platform - Python programming activity
 Sugar Learning Platform promotes collaborative learning through Sugar
 Activities that encourage critical thinking, the heart of a quality
 education.  Designed from the ground up especially for children, Sugar
 offers an alternative to traditional “office-desktop” software.
 .
 Learner applications in Sugar are called Activities.  They are software
 packages that automatically save your work - producing specific
 instances of the Activity that can be resumed at a later time.  Many
 Activities support learner collaboration, where multiple learners may
 be invited to join a collective Activity session.
 .
 Pippy allows the student to examine, execute, and modify simple Python
 programs.  In addition it is possible to write Python statements to
 play sounds, calculate  expressions, or make simple text based
 interactive games.
